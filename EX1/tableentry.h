#ifndef TABLEENTRY_H
#define TABLEENTRY_H

#include <QString>
#include <QDateTime>

struct tableEntry {
  QDateTime datetime;
  QString nameOfCar;
  int weight;
  
  QString toString() {
    return datetime.toString("dd.MM.yyyy h:mm") + ", " +
        nameOfCar + ", " + QString::number(weight);
  }
};

typedef QMap<QString, QVector<tableEntry>> CarsToEntriesMap;

#endif // TABLEENTRY_H
