#include "tablemodel.h"

TableModel::TableModel(CarsToEntriesMap data, QObject *parent)
  : QObject(parent),
    carsToEntries(data)
{
  // Сортировка вставками
  for (auto entries_it = carsToEntries.begin(); entries_it != carsToEntries.end(); ++entries_it) {
    auto &entry = *entries_it;
    for (int i = 1; i < entry.size(); ++i) {
      for (int j = i; j > 0 && entry[j - 1].datetime > entry[j].datetime; --j)
        std::swap(entry[j - 1], entry[j]);
    }
  }
}

void TableModel::writeDataToFiles(QString folderPath)
{
  for (auto it = carsToEntries.keyBegin(); it != carsToEntries.keyEnd(); ++it) {
    QFile file(folderPath + QDir::separator() + *it + ".txt");
    file.open(QFile::WriteOnly);
    
    QTextStream fileStream(&file);
    
    int weightSum = 0;
    
    for (auto entries_it = carsToEntries[*it].begin(); entries_it != carsToEntries[*it].end(); ++entries_it) {
      weightSum += entries_it->weight;
      fileStream << entries_it->toString() + '\n';
    }
    
    double finalWeight = weightSum / carsToEntries[*it].size();
    fileStream << QString("Общий итог по массе: ") << finalWeight;
  }
}
