#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QMessageBox>
#include <QtDebug>

#include "tableparser.h"
#include "tablemodel.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT
  
public:
  explicit MainWindow(QWidget *parent = nullptr);
  ~MainWindow();
  
private slots:
  void on_openTableBtn_clicked();
  
  void on_selectFolderBtn_clicked();
  
  void on_sortBtn_clicked();
  
private:
  Ui::MainWindow *ui;
  
  QString tableFileName;
  QString folderName;
  
  TableParser *tableParser;
};

#endif // MAINWINDOW_H
