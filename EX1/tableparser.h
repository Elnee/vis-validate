#ifndef TABLEPARSER_H
#define TABLEPARSER_H

#include <QObject>
#include <QFile>
#include <QMap>
#include <QVector>
#include <QTextStream>

#include "tableentry.h"

class TableParser : public QObject
{
  Q_OBJECT
public:
  explicit TableParser(QString tableFilePath, QObject *parent = nullptr);
  QString getFileContent(); 
  CarsToEntriesMap getParsedData();
  
signals:
  
public slots:
  
private:
  QFile tableFile;
  CarsToEntriesMap carsToEntries;
};

#endif // TABLEPARSER_H
