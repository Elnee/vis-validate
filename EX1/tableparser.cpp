#include "tableparser.h"

TableParser::TableParser(QString tableFilePath, QObject *parent) 
  : QObject(parent),
    tableFile(tableFilePath)
{
  // Open file
  tableFile.open(QFile::ReadOnly | QFile::Text);
  
  // Parsing data
  tableFile.seek(0);
  while (!tableFile.atEnd()) {
    QString line = tableFile.readLine();
    if (line.isEmpty() || line == '\n') continue;
    
    QStringList entryList = line.split(',', QString::SkipEmptyParts);
    
    tableEntry te;
    
    // Парсинг данных
    te.datetime = QDateTime::fromString(entryList[0].simplified(), "dd.MM.yyyy h:mm");
    te.nameOfCar = entryList[1].simplified();
    te.weight = entryList[2].simplified().toInt();
    
    carsToEntries[te.nameOfCar].push_back(te);
  }
}

QString TableParser::getFileContent()
{
  tableFile.seek(0);
  QTextStream tableFileStream(&tableFile);
  return tableFileStream.readAll();
}

CarsToEntriesMap TableParser::getParsedData()
{
  return carsToEntries;
}
