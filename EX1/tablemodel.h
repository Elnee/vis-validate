#ifndef TABLEMODEL_H
#define TABLEMODEL_H

#include <QObject>
#include <QMap>
#include <QVector>
#include <QFile>
#include <QTextStream>
#include <QDir>

#include "tableentry.h"

class TableModel : public QObject
{
  Q_OBJECT
public:
  explicit TableModel(CarsToEntriesMap data, QObject *parent = nullptr);
  void writeDataToFiles(QString folderPath);
  
signals:
  
public slots:
  
private:
  CarsToEntriesMap carsToEntries;
};

#endif // TABLEMODEL_H
