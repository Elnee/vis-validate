#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::on_openTableBtn_clicked()
{
  QString temp = QFileDialog::getOpenFileName(this, "Выберите файл", QDir::homePath(), "Текстовые файлы (*.txt)");
  if (temp.isEmpty()) return; // Выйти из функции если файл не был выбран
  tableFileName = temp;
  
  ui->tableFileLabel->setText(tableFileName);
  
  // Загрузка текстового файла в QPlainTextEdit
  if (tableParser != nullptr) delete tableParser;
  tableParser = new TableParser(tableFileName, this);
  ui->tableTextEdit->appendPlainText(tableParser->getFileContent());
}

void MainWindow::on_selectFolderBtn_clicked()
{
  QString temp = QFileDialog::getExistingDirectory(this, "Выберите директорию", 
                                                 QDir::homePath(),
                                                 QFileDialog::ShowDirsOnly 
                                                 | QFileDialog::DontResolveSymlinks);
  if (temp.isEmpty()) return; // Выйти из функции если файл не выбран
  folderName = temp;
  ui->folderNameLabel->setText(folderName);
}

void MainWindow::on_sortBtn_clicked()
{
  if (tableFileName.isEmpty() || folderName.isEmpty()) {
    QMessageBox alertBox;
    alertBox.setText("Необходимо выбрать файл таблицы и каталог для сохранения результатов!");
    alertBox.setIcon(QMessageBox::Icon::Warning);
    alertBox.exec();
    return;
  }
  
  TableModel tableModel(tableParser->getParsedData(), this);
  tableModel.writeDataToFiles(folderName);
  
  ui->statusBar->showMessage("Выполнено!");
}
