#ifndef ABSTRACTSENSOR_H
#define ABSTRACTSENSOR_H

#include <QObject>

class AbstractSensor : public QObject
{
  Q_OBJECT
public:
  explicit AbstractSensor(QObject *parent = nullptr);
  
signals:
  void triggered();      // Сигнал отправляется при достижении  
  void changedEnabled(); // Сигнал отправляется при включении/выключении датчика
  
public slots:
  virtual void setValue(int value) = 0;               // Установка текущего значения датчика
  virtual void setTriggerValue(int value) = 0;        // Установка значения, при котором датчик сработает
  virtual void setMinMaxValues(int min, int max) = 0; // Установка диапазона работы датчика
  virtual void setEnabled(bool state) = 0;            // Включение/выключение датчика
  
  virtual void getValue() const = 0;                  // Получить текущее значение
  virtual void getTriggerValue() const = 0;           // Получить значение, при котором датчик сработает
  virtual void getMinMaxValues() const = 0;           // Получить диапазон работы датчика
  virtual void getEnabled() const = 0;                // Получить состояние датчика (включен/выключен)
};

#endif // ABSTRACTSENSOR_H
