#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>

#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlDriver>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>

#include <QStringList>
#include <QDate>

#include <QtDebug>

class Database : public QObject
{
  Q_OBJECT
public:
  explicit Database(QObject *parent = nullptr);
  QStringList getUsers();
  int workedTimeInSecsForDate(const QDate &date, const QString userInfo);
  
private:
  void connect();
  void init();
  QSqlDatabase db;
  QSqlQuery executeQuery(QString query);
};

#endif // DATABASE_H
