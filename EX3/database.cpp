#include "database.h"

Database::Database(QObject *parent) : QObject(parent)
{
  connect();
  init();
}

void Database::connect()
{
  const QString driver = "QSQLITE";
  
  if (QSqlDatabase::isDriverAvailable(driver)) {
    db = QSqlDatabase::addDatabase(driver);
    db.setDatabaseName(":memory:");
    if (!db.open()) 
      qWarning() << "Database connection error: " << db.lastError().text();
  } else {
    qWarning() << "Database driver doesn't exist!";
  }
}

void Database::init()
{
  // Создание таблиц
  executeQuery("CREATE TABLE archive (id INTEGER PRIMARY KEY, time TEXT, card_id INTEGER)");
  executeQuery("CREATE TABLE card (id INTEGER PRIMARY KEY, info TEXT)");
  
  // Заполнение тестовыми значениями
  // Заполнение таблицы card
  executeQuery("INSERT INTO card(info) VALUES('Иванов Пётр Алексеевич')");
  executeQuery("INSERT INTO card(info) VALUES('Колесов Александр Семенович')");
  executeQuery("INSERT INTO card(info) VALUES('Доронин Орест Рудольфович')");
  executeQuery("INSERT INTO card(info) VALUES('Федосеев Алексей Германович')");
  executeQuery("INSERT INTO card(info) VALUES('Тихонов Мартин Иосифович')");
  
  // Заполнение таблицы archive
  executeQuery("INSERT INTO archive(time, card_id) VALUES('2019-06-04 08:23', 1)");
  executeQuery("INSERT INTO archive(time, card_id) VALUES('2019-06-04 16:30', 1)");
  
  executeQuery("INSERT INTO archive(time, card_id) VALUES('2019-06-05 09:23', 2)");
  executeQuery("INSERT INTO archive(time, card_id) VALUES('2019-06-05 17:30', 2)");
  
  executeQuery("INSERT INTO archive(time, card_id) VALUES('2019-06-10 10:02', 3)");
  executeQuery("INSERT INTO archive(time, card_id) VALUES('2019-06-10 18:30', 3)");
  
  executeQuery("INSERT INTO archive(time, card_id) VALUES('2019-06-12 08:10', 4)");
  executeQuery("INSERT INTO archive(time, card_id) VALUES('2019-06-12 14:30', 4)");
  
  executeQuery("INSERT INTO archive(time, card_id) VALUES('2019-06-03 7:23', 5)");
  executeQuery("INSERT INTO archive(time, card_id) VALUES('2019-06-03 16:00', 5)");
}

QStringList Database::getUsers()
{
  QStringList users;
  QSqlQuery selectQuery = executeQuery("SELECT info FROM card");
  while (selectQuery.next()) {
    users.append(selectQuery.value(0).toString());
  }
  return users;
}

int Database::workedTimeInSecsForDate(const QDate &date, const QString userInfo)
{
  QString dateStr = date.toString("yyyy-MM-dd");
  QSqlQuery query;
  
  QTime coming;
  QTime leaving;
  
  query.prepare("SELECT time(archive.time) "
                "FROM archive "
                "INNER JOIN card ON archive.card_id = card.id "
                "WHERE card.info = ? AND date(archive.time) = ? "
                "ORDER BY archive.time ASC");
  
  query.bindValue(0, userInfo);
  query.bindValue(1, dateStr);
  
  if (!query.exec())
    qWarning() << "Error executing SQL query: " << query.lastError().text();
  
  if (query.first()) {
    coming = QTime::fromString(query.value(0).toString(), "hh:mm:ss");
    query.next();
    leaving = QTime::fromString(query.value(0).toString(), "hh:mm:ss");
  } else {
    return 0;
  }
  
  return coming.secsTo(leaving);
}

QSqlQuery Database::executeQuery(QString query)
{
  QSqlQuery sqlQuery;
  if (!sqlQuery.exec(query))
    qWarning() << "Error executing SQL query: " << sqlQuery.lastError().text();
  
  return sqlQuery;
}
