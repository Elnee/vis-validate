В ComboBox выбирается ФИО сотрудника и после чего можно выбрать дату на календаре.  
Если записей в этот день у сотрудника не было, то будет написано "Нет информации".  
В противном случае будет выведена информация сколько часов и минут было отработано  
в этот день.

В данной задаче для примера я использовал СУБД SQLite и интересную её особенность  
создания БД прямо в памяти. Метод `init()` создаёт таблицы и заполняет  
базу тестовыми значениями.

Поле `card_id` в таблице `archive` следовало бы сделать внешним ключом, но такая  
возмонжость отсутсвует в SQLite по умолчанию. Также SQLite не имеет специальных  
типов для хранения даты, но имеет для этого специальные функции, работающие с  
INTEGER, TEXT и REAL. В данном случае я использовал TEXT для хранения времени,  
так как хранимое время в данном типе более читаемое.
