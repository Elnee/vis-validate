#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStringListModel>
#include <QTime>

#include "database.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT
  
public:
  explicit MainWindow(QWidget *parent = nullptr);
  ~MainWindow();
  
private slots:
  void on_calendarWidget_clicked(const QDate &date);
  
  void on_employeesComboBox_currentIndexChanged(int index);
  
private:
  Ui::MainWindow *ui;
  QStringListModel *employeesModel;  
  Database db;
};

#endif // MAINWINDOW_H
