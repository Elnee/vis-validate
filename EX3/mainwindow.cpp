#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow),
  db(this)
{
  ui->setupUi(this);
  
  employeesModel = new QStringListModel(db.getUsers());
  ui->employeesComboBox->setModel(employeesModel);
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::on_calendarWidget_clicked(const QDate &date)
{
  int workedTime = db.workedTimeInSecsForDate(date, ui->employeesComboBox->currentText());
  
  if (workedTime > 0) {
    QTime time(0, 0, 0);
    time = time.addSecs(workedTime);
    ui->workedTimeLabel->setText("В этот день отработано " + 
                                 time.toString("hh") + " часов и " +
                                 time.toString("mm") + " минут");
  } else {
    ui->workedTimeLabel->setText("Нет информации");
  }
}

void MainWindow::on_employeesComboBox_currentIndexChanged(int index)
{
  ui->workedTimeLabel->clear();
}
